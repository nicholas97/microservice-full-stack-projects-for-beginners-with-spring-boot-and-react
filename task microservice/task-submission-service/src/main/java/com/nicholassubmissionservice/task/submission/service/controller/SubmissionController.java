package com.nicholassubmissionservice.task.submission.service.controller;

import com.nicholassubmissionservice.task.submission.service.model.Submission;
import com.nicholassubmissionservice.task.submission.service.model.TaskDto;
import com.nicholassubmissionservice.task.submission.service.model.TaskStatus;
import com.nicholassubmissionservice.task.submission.service.model.UserDto;
import com.nicholassubmissionservice.task.submission.service.request.*;
import com.nicholassubmissionservice.task.submission.service.service.SubmissionService;
import com.nicholassubmissionservice.task.submission.service.service.TaskService;
import com.nicholassubmissionservice.task.submission.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/submission")
public class SubmissionController {
    @Autowired
    private SubmissionService submissionService;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    @PostMapping("/addSubmission")
    public ResponseEntity<Submission> addSubmission(@RequestBody SubmitTaskRequest request, @RequestHeader("Authorization") String token) throws Exception{
        UserDto user = userService.getUserProfile(token);
        GetTaskRequest getTaskRequestrequest = new GetTaskRequest();
        getTaskRequestrequest.setTaskId(request.getTaskId());

        TaskDto task = taskService.getTaskById(getTaskRequestrequest,token);
        if(task==null){
            throw new Exception("Task not found with id : "+request.getTaskId());

        }
        Submission submission = submissionService.addSubmission(request.getTaskId(), request.getGitLink(), user.getId(),token);
        return new ResponseEntity<>(submission, HttpStatus.OK);

    }
    @PostMapping("/getSubmission")
    public ResponseEntity<Submission> getSubmission(@RequestBody GetSubmissionRequest request, @RequestHeader("Authorization") String token) throws Exception{
        Submission submission = submissionService.getSubmissionById(request.getSubmissionId());
        return new ResponseEntity<>(submission,HttpStatus.OK);
    }
    @PostMapping("/getSubmissions")
    public ResponseEntity<List<Submission>> getSubmissions(@RequestBody GetSubmissionsRequest request, @RequestHeader("Authorization") String token) throws Exception{
        List<Submission> submissions = submissionService.getSubmissions();
        return new ResponseEntity<>(submissions,HttpStatus.OK);
    }
    @PostMapping("/getTaskSubmissions")
    public ResponseEntity<List<Submission>> getTaskSubmissions(@RequestBody GetTaskSubmissionsRequest request, @RequestHeader("Authorization") String token) throws Exception{
        List<Submission> submissions = submissionService.getSubmissionsByTaskId(request.getTaskId());
        return new ResponseEntity<>(submissions,HttpStatus.OK);
    }
    @PutMapping("/updateSubmissionStatus")
    public  ResponseEntity<Submission> updateSubmissionStatus(@RequestBody AcceptDeclineSubmissionRequest request, @RequestHeader("Authorization") String token) throws Exception{
        Submission submission = submissionService.updateSubmissionStatus(request.getSubmissionId(), request.getStatus(),token);
        if(request.getStatus().equals("ACCEPT")) {
            UpdateTaskRequest updateTaskRequestrequest = new UpdateTaskRequest();
            updateTaskRequestrequest.setTaskId(submission.getTaskId());
            updateTaskRequestrequest.setStatus(TaskStatus.DONE);
            taskService.updateTask(updateTaskRequestrequest, token);
        }
        return new ResponseEntity<>(submission, HttpStatus.OK);
    }

}
