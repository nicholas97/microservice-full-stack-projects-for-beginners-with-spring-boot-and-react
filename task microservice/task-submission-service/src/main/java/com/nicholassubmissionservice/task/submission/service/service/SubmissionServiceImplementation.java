package com.nicholassubmissionservice.task.submission.service.service;

import com.nicholassubmissionservice.task.submission.service.model.Submission;
import com.nicholassubmissionservice.task.submission.service.model.TaskDto;
import com.nicholassubmissionservice.task.submission.service.model.TaskStatus;
import com.nicholassubmissionservice.task.submission.service.repository.SubmissionRepository;
import com.nicholassubmissionservice.task.submission.service.request.GetTaskRequest;
import com.nicholassubmissionservice.task.submission.service.request.UpdateTaskRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SubmissionServiceImplementation implements SubmissionService{

    @Autowired
    private SubmissionRepository submissionRepository;

    @Override
    public Submission addSubmission(Long taskId, String gitLink, Long userId, String token) throws Exception {
        Submission submission = new Submission();
        submission.setTaskId(taskId);
        submission.setUserId(userId);
        submission.setGitLink(gitLink);
        submission.setSubmissionTime(LocalDateTime.now());
        return submissionRepository.save(submission);
    }

    @Override
    public Submission getSubmissionById(Long submissionId) throws Exception {
        return submissionRepository.findById(submissionId).orElseThrow(()->new Exception("Task Submission not found with id : " +submissionId));
    }

    @Override
    public List<Submission> getSubmissions() {
        return submissionRepository.findAll();
    }

    @Override
    public List<Submission> getSubmissionsByTaskId(Long taskId) {
        return submissionRepository.findByTaskId(taskId);
    }

    @Override
    public Submission updateSubmissionStatus(Long id, String status, String token) throws Exception {
        Submission submission = getSubmissionById(id);
        submission.setStatus(status);
        return submissionRepository.save(submission);
    }
}
