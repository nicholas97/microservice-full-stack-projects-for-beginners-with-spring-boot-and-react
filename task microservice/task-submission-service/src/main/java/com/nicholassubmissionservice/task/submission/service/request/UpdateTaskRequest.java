package com.nicholassubmissionservice.task.submission.service.request;

import com.nicholassubmissionservice.task.submission.service.model.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateTaskRequest {
    private Long taskId;

    private String title;
    private String description;
    private String image;
    private List<String> tags = new ArrayList<>();
    private TaskStatus status;
    private LocalDateTime deadline;
}