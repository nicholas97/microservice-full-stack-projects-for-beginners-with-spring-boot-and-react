package com.nicholassubmissionservice.task.submission.service.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetSubmissionsRequest {
}
