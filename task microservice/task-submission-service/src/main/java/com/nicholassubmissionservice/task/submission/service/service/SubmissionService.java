package com.nicholassubmissionservice.task.submission.service.service;

import com.nicholassubmissionservice.task.submission.service.model.Submission;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SubmissionService {
    Submission addSubmission(Long taskId,String gitLink, Long userId, String token)throws Exception;

    Submission getSubmissionById(Long submissionId) throws Exception;

    List<Submission> getSubmissions();

    List<Submission> getSubmissionsByTaskId(Long taskId);

    Submission updateSubmissionStatus(Long id, String status, String token) throws Exception;
}
