package com.nicholassubmissionservice.task.submission.service.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AcceptDeclineSubmissionRequest {
    private Long submissionId;
    private String status;
}
