package com.nicholassubmissionservice.task.submission.service.service;

import com.nicholassubmissionservice.task.submission.service.model.TaskDto;
import com.nicholassubmissionservice.task.submission.service.request.GetTaskRequest;
import com.nicholassubmissionservice.task.submission.service.request.UpdateTaskRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "TASK-SERVICE",url = "http://localhost:5002")
public interface TaskService {

    @PostMapping("api/task/getTask")
    public TaskDto getTaskById(@RequestBody GetTaskRequest request, @RequestHeader("Authorization") String token) throws Exception;

    @PutMapping("api/task/updateTask")
    public TaskDto updateTask(@RequestBody UpdateTaskRequest request, @RequestHeader("Authorization") String token) throws Exception;
}
