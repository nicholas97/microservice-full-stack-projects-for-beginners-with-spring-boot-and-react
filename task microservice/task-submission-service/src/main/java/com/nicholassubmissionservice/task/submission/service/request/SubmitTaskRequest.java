package com.nicholassubmissionservice.task.submission.service.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubmitTaskRequest {
    private Long taskId;
    private String gitLink;
}
