package com.nicholastaskservice.task.service.service;

import com.nicholastaskservice.task.service.model.Task;
import com.nicholastaskservice.task.service.model.TaskStatus;

import java.util.List;

public interface TaskService {
    Task createTask(Task task, String requesterRole)throws Exception;

    Task getTaskById(Long id)throws Exception;

    List<Task> getTasks(TaskStatus status);

    Task updateTask( Task updatedTask) throws Exception;

    void deleteTask(Long id) throws Exception;

    Task assignToUser(Long userId,Long taskId) throws Exception;

    List<Task> assignedUsersTask(Long userId, TaskStatus status);

}
