package com.nicholastaskservice.task.service.request;

import com.nicholastaskservice.task.service.model.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetMyTasksRequest {
    private TaskStatus status;
}
