package com.nicholastaskservice.task.service.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssignTaskRequest {
    private Long taskId;
    private Long userId;
}
