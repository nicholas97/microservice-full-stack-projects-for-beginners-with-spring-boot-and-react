package com.nicholastaskservice.task.service.service;

import com.nicholastaskservice.task.service.model.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "USER-SERVICE",url = "http://localhost:5001")
public interface UserService {

    @GetMapping("api/user/myProfile")
    public UserDto getUserProfile(@RequestHeader("Authorization") String token);
}
