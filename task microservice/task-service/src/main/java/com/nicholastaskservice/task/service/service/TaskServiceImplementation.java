package com.nicholastaskservice.task.service.service;

import com.nicholastaskservice.task.service.model.Task;
import com.nicholastaskservice.task.service.model.TaskStatus;
import com.nicholastaskservice.task.service.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskServiceImplementation implements TaskService {
    @Autowired
    private TaskRepository repository;

    @Override
    public Task createTask(Task task, String requesterRole) throws Exception {
        if(!requesterRole.equals("ROLE_ADMIN")){
            throw new Exception("Only admin can create task");
        }
        task.setStatus(TaskStatus.PENDING);
        task.setCreatedAt(LocalDateTime.now());
        return repository.save(task);
    }

    @Override
    public Task getTaskById(Long id) throws Exception {
        return repository.findById(id).orElseThrow(()->new Exception("Task not found with id "+id));
    }

    @Override
    public List<Task> getTasks(TaskStatus status) {
        List<Task> tasks = repository.findAll();
        List<Task> filteredTasks = tasks.stream().filter(
                task -> status==null||task.getStatus().name().equalsIgnoreCase(status.toString())

        ).collect(Collectors.toList());

        return filteredTasks;
    }

    @Override
    public Task updateTask(Task updatedTask) throws Exception {
        Task existingTask = getTaskById(updatedTask.getId());
        if(updatedTask.getTitle()!=null){
            existingTask.setTitle(updatedTask.getTitle());
        }
        if(updatedTask.getImage()!=null){
            existingTask.setImage(updatedTask.getImage());
        }
        if(updatedTask.getDescription()!=null){
            existingTask.setDescription(updatedTask.getDescription());
        }
        if(updatedTask.getTags()!=null){
            existingTask.setTags(updatedTask.getTags());
        }
        if(updatedTask.getStatus()!=null){
            existingTask.setStatus(updatedTask.getStatus());
        }
        if(updatedTask.getDeadline()!=null){
            existingTask.setDeadline(updatedTask.getDeadline());
        }

        return repository.save(existingTask);
    }

    @Override
    public void deleteTask(Long id) throws Exception {
        getTaskById(id);
        repository.deleteById(id);

    }

    @Override
    public Task assignToUser(Long userId, Long taskId) throws Exception {
        Task task = getTaskById(taskId);
        task.setAssignedUserId(userId);
        task.setStatus(TaskStatus.ASSIGNED);
        return repository.save(task);
    }

    @Override
    public List<Task> assignedUsersTask(Long userId, TaskStatus status) {
        List<Task> tasks = repository.findByAssignedUserId(userId);

        List<Task> filteredTasks = tasks.stream().filter(
                task -> status==null||task.getStatus().name().equalsIgnoreCase(status.toString())

        ).collect(Collectors.toList());

        return filteredTasks;
    }

}
