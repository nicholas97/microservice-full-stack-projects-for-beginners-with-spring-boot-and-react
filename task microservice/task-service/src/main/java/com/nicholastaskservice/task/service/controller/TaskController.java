package com.nicholastaskservice.task.service.controller;

import com.nicholastaskservice.task.service.model.Task;
import com.nicholastaskservice.task.service.model.UserDto;
import com.nicholastaskservice.task.service.request.*;
import com.nicholastaskservice.task.service.service.TaskService;
import com.nicholastaskservice.task.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    @PostMapping("/addTask")
    public ResponseEntity<Task> createTask(@RequestBody Task task, @RequestHeader("Authorization") String token) throws Exception{
        UserDto userDto = userService.getUserProfile(token);
        Task newTask = taskService.createTask(task,userDto.getRole());

        return new ResponseEntity<>(newTask, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/getTask", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<Task> getTaskById(@RequestBody GetTaskRequest request, @RequestHeader("Authorization") String token) throws Exception{

        Task newTask = taskService.getTaskById(request.getTaskId());

        return new ResponseEntity<>(newTask, HttpStatus.OK);
    }
    @PostMapping("/getMyTasks")
    public ResponseEntity<List<Task>> getMyTasks(@RequestBody GetMyTasksRequest request, @RequestHeader("Authorization") String token) throws Exception{
        System.out.println("getMyTasks"+request.toString());
        UserDto userDto = userService.getUserProfile(token);
        List<Task> tasks = taskService.assignedUsersTask(userDto.getId(),request.getStatus());

        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }
    @PostMapping("/getTasks")
    public ResponseEntity<List<Task>> getTasks(@RequestBody GetTasksRequest request, @RequestHeader("Authorization") String token) throws Exception{
        System.out.println("getTasks"+request.toString());
        List<Task> tasks = taskService.getTasks(request.getStatus());

        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }
    @PutMapping("/assignTask")
    public ResponseEntity<Task> assignTaskToUser(@RequestBody AssignTaskRequest request, @RequestHeader("Authorization") String token) throws Exception{

        Task task = taskService.assignToUser(request.getUserId(),request.getTaskId());

        return new ResponseEntity<>(task, HttpStatus.OK);
    }
    @PutMapping("/updateTask")
    public ResponseEntity<Task> updateTask(@RequestBody UpdateTaskRequest request, @RequestHeader("Authorization") String token) throws Exception{

        Task updateTask = new Task();
        updateTask.setTitle(request.getTitle());
        updateTask.setDescription(request.getDescription());
        updateTask.setStatus(request.getStatus());
        updateTask.setImage(request.getImage());
        updateTask.setTags(request.getTags());
        updateTask.setId(request.getTaskId());
        updateTask.setDeadline(request.getDeadline());
        Task task = taskService.updateTask(updateTask);

        return new ResponseEntity<>(task, HttpStatus.OK);
    }
    @DeleteMapping("/delete")
    public ResponseEntity<Long> deleteTask(@RequestBody DeleteTaskRequest request) throws Exception{
        taskService.deleteTask(request.getTaskId());

        return new ResponseEntity<>(request.getTaskId(),HttpStatus.OK);
    }
}
