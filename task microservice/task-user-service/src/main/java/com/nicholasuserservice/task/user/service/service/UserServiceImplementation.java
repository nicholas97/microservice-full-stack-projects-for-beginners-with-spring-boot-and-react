package com.nicholasuserservice.task.user.service.service;

import com.nicholasuserservice.task.user.service.config.JwtProvider;
import com.nicholasuserservice.task.user.service.model.User;
import com.nicholasuserservice.task.user.service.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplementation implements UserService{
    @Autowired
    private UserRepository userRepository;
    @Override
    public User getUser(String token) {
        String email = JwtProvider.getEmailFromJwtToken(token);
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();

    }
}
