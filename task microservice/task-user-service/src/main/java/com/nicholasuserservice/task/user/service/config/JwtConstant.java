package com.nicholasuserservice.task.user.service.config;

public class JwtConstant {
    public static final String SECRET_KEY = "W0jkyDxVmK3nrDlHkM6Spu+haQTqYKH3IpWvDoPDQtIpBAC5bguQIdIO63fOkHMH";
    public static final String JWT_HEADER = "Authorization";
}
