package com.nicholasuserservice.task.user.service.service;

import com.nicholasuserservice.task.user.service.model.User;

import java.util.List;

public interface UserService {
    public User getUser(String token);
    public List<User> getUsers();
}
