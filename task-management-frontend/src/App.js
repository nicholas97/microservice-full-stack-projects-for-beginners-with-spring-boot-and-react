import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ThemeProvider } from '@mui/material';
import { darkTheme } from './theme/darktheme';
import NavBar from './pages/NavBar/NavBar';
import Home from './pages/Home/Home';
import Auth from './pages/Auth/Auth';
import './App.css';
import { getTasks } from './redux/TaskSlice';
import { getUserProfile } from './redux/AuthSlice';

function App() {
  const user = true;
  const dispatch = useDispatch();

  const { task, auth } = useSelector((store) => store);
  useEffect(() => {
    console.log('App useEffect');
    dispatch(getUserProfile(auth.token || localStorage.getItem('token')));
    dispatch(
      getTasks({
        status: 'PENDING',
      })
    );
  }, [auth.token]);

  return (
    <ThemeProvider theme={darkTheme}>
      {auth.user ? (
        <div>
          <NavBar />
          <Home />
        </div>
      ) : (
        <Auth />
      )}
    </ThemeProvider>
  );
}

export default App;
