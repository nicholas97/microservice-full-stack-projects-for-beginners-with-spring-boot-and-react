import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { BASE_URL, api, setAuthHeader } from '../api/api';

export const getTasks = createAsyncThunk('task/getTasks', async (request) => {
  setAuthHeader(localStorage.getItem('token'), api);
  try {
    console.log('getTasks request', request);
    const { data } = await api.post(`/api/task/getTasks`, request);
    console.log('getTasks success', data);
    return data;
  } catch (error) {
    console.log('catch error', error);
    throw Error(error.response.data.error);
  }
});

export const getMyTasks = createAsyncThunk(
  'task/getMyTasks',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.post(`/api/task/getMyTasks`, request);
      console.log('getMyTasks success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);
export const getTask = createAsyncThunk('task/getTask', async (request) => {
  setAuthHeader(localStorage.getItem('token'), api);
  try {
    const { data } = await api.post(`/api/task/getTask`, request);
    console.log('getTask success', data);
    return data;
  } catch (error) {
    console.log('catch error', error);
    throw Error(error.response.data.error);
  }
});

export const addTask = createAsyncThunk('task/addTask', async (request) => {
  setAuthHeader(localStorage.getItem('token'), api);
  try {
    const { data } = await api.post(`/api/task/addTask`, request);
    console.log('addTask success', data);
    return data;
  } catch (error) {
    console.log('catch error', error);
    throw Error(error.response.data.error);
  }
});

export const updateTask = createAsyncThunk(
  'task/updateTask',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.put(`/api/task/updateTask`, request);
      console.log('updateTask success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

export const assignTask = createAsyncThunk(
  'task/assignTask',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.put(`/api/task/assignTask`, request);
      console.log('assignTask success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

export const deleteTask = createAsyncThunk(
  'task/deleteTask',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.delete(`/api/task/delete`, { data: request });
      console.log('deleteTask success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

const taskSlice = createSlice({
  name: 'task',
  initialState: {
    tasks: [],
    loading: false,
    error: null,
    taskDetails: null,
    userTasks: [],
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getTasks.pending, (state) => {
        state.loading = true;
        state.error = null;
        console.log('getTasks.pending');
      })
      .addCase(getTasks.fulfilled, (state, action) => {
        state.loading = false;
        state.tasks = action.payload;
        console.log('getTasks.fulfilled');
      })
      .addCase(getTasks.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        console.log('getTasks.rejected');
      })
      .addCase(getMyTasks.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getMyTasks.fulfilled, (state, action) => {
        state.loading = false;
        state.userTasks = action.payload;
      })
      .addCase(getMyTasks.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(getTask.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getTask.fulfilled, (state, action) => {
        state.loading = false;
        state.taskDetails = action.payload;
      })
      .addCase(getTask.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(addTask.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(addTask.fulfilled, (state, action) => {
        state.loading = false;
        state.tasks.push(action.payload);
      })
      .addCase(addTask.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(updateTask.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(updateTask.fulfilled, (state, action) => {
        state.loading = false;
        const updatedTask = action.payload;
        state.tasks = state.tasks.map((task) =>
          task.id === updatedTask.id ? { ...task, ...updatedTask } : task
        );
      })
      .addCase(updateTask.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(assignTask.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(assignTask.fulfilled, (state, action) => {
        state.loading = false;
        const asignedTask = action.payload;
        state.tasks = state.tasks.map((task) =>
          task.id === asignedTask.id ? { ...task, ...asignedTask } : task
        );
      })
      .addCase(assignTask.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(deleteTask.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(deleteTask.fulfilled, (state, action) => {
        state.loading = false;
        state.tasks = state.tasks.filter((task) => task.id !== action.payload);
      })
      .addCase(deleteTask.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});
export default taskSlice.reducer;
