import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { BASE_URL, api, setAuthHeader } from '../api/api';

export const addSubmission = createAsyncThunk(
  'submission/addSubmission',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.post(`/api/submission/addSubmission`, request);
      console.log('addSubmission success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

export const getSubmission = createAsyncThunk(
  'submission/getSubmission',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.post(`/api/submission/getSubmission`, {
        request,
      });
      console.log('getSubmission success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

export const getSubmissions = createAsyncThunk(
  'submission/getSubmissions',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.post(`/api/submission/getSubmissions`, {
        request,
      });
      console.log('getSubmissions success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

export const getTaskSubmissions = createAsyncThunk(
  'submission/getTaskSubmissions',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.post(
        `/api/submission/getTaskSubmissions`,
        request
      );
      console.log('getTaskSubmissions success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

export const updateSubmissionStatus = createAsyncThunk(
  'submission/updateSubmissionStatus',
  async (request) => {
    setAuthHeader(localStorage.getItem('token'), api);
    try {
      const { data } = await api.put(
        `/api/submission/updateSubmissionStatus`,
        request
      );
      console.log('updateSubmissionStatus success', data);
      return data;
    } catch (error) {
      console.log('catch error', error);
      throw Error(error.response.data.error);
    }
  }
);

const submissionSlice = createSlice({
  name: 'submission',
  initialState: {
    submissions: [],
    loading: false,
    error: null,
    submissionDetails: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addSubmission.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(addSubmission.fulfilled, (state, action) => {
        state.submissions.push(action.payload);
        state.loading = false;
      })
      .addCase(addSubmission.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(getSubmission.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getSubmission.fulfilled, (state, action) => {
        state.submissionDetails = action.payload;
        state.loading = false;
      })
      .addCase(getSubmission.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(getSubmissions.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getSubmissions.fulfilled, (state, action) => {
        state.submissions = action.payload;
        state.loading = false;
      })
      .addCase(getSubmissions.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(getTaskSubmissions.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getTaskSubmissions.fulfilled, (state, action) => {
        state.submissions = action.payload;
        state.loading = false;
      })
      .addCase(getTaskSubmissions.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(updateSubmissionStatus.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(updateSubmissionStatus.fulfilled, (state, action) => {
        state.loading = false;
        const updatedSubmission = action.payload;
        state.submissions = state.submissions.map((submission) =>
          submission.id === updatedSubmission.id
            ? { ...submission, ...updatedSubmission }
            : submission
        );
      })
      .addCase(updateSubmissionStatus.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});
export default submissionSlice.reducer;
