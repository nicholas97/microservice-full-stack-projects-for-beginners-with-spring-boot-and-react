import React, { useEffect } from 'react';
import TaskCard from '../TaskCard/TaskCard';
import { useDispatch, useSelector } from 'react-redux';
import { getMyTasks, getTasks } from '../../redux/TaskSlice';
import { useLocation } from 'react-router-dom';

export default function TaskList() {
  const dispatch = useDispatch();
  const { task, auth } = useSelector((store) => store);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const filterValue = queryParams.get('filter');

  useEffect(() => {
    if (auth.user?.role === 'ROLE_ADMIN') {
      dispatch(
        getTasks({
          status: filterValue,
        })
      );
    } else {
      dispatch(
        getMyTasks({
          status: filterValue,
        })
      );
    }
  }, [filterValue]);

  return (
    <div className='space-y-5 w-[67vw]'>
      <div className='space-y-3'>
        {auth.user?.role === 'ROLE_ADMIN'
          ? task.tasks.map((item, index) => (
              <div key={item.toString() + index}>
                <TaskCard task={item} />
              </div>
            ))
          : task.userTasks.map((item, index) => (
              <div key={item.toString() + index}>
                <TaskCard task={item} />
              </div>
            ))}
      </div>
    </div>
  );
}
