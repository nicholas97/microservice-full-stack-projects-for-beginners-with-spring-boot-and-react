import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Modal,
  Box,
  Grid,
  TextField,
  Autocomplete,
  Button,
} from '@mui/material';
import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { addTask } from '../../redux/TaskSlice';
import { addSubmission } from '../../redux/SubmissionSlice';
import { useLocation, useNavigate } from 'react-router-dom';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  outline: 'none',
  boxShadow: 24,
  p: 4,
};

const tags = ['Angular', 'React', 'Vuejs', 'Spring boot', 'Node'];

export default function SubmissionCreateForm({ handleClose, open }) {
  const dispatch = useDispatch();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const taskId = queryParams.get('taskId');
  const [formData, setFormData] = useState({
    taskId: taskId,
    gitLink: '',
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    formData.taskId = Number(taskId);
    dispatch(addSubmission(formData));
    console.log(formData);
    handleClose();
  };
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <form onSubmit={handleSubmit}>
            <Grid
              container
              spacing={2}
              alignItems={'center'}
            >
              <Grid
                item
                xs={12}
              >
                <TextField
                  label={'Git Link'}
                  fullWidth
                  name='gitLink'
                  value={formData.gitLink}
                  onChange={handleChange}
                />
              </Grid>
              <Grid
                item
                xs={12}
              >
                <Button
                  fullWidth
                  className='customBtn'
                  sx={{ padding: '.9rem' }}
                  type='submit'
                >
                  Submit
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div>
  );
}
