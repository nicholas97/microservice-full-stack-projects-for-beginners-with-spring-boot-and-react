import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Modal,
  Box,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Button,
  Divider,
} from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { getUserList } from '../../redux/AuthSlice';
import { assignTask } from '../../redux/TaskSlice';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  outline: 'none',
  boxShadow: 24,
  p: 2,
};

export default function UserList({ handleClose, open }) {
  const dispatch = useDispatch();
  const { auth } = useSelector((store) => store);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const taskId = queryParams.get('taskId');
  const handleAssignedTask = (user) => {
    dispatch(assignTask({ taskId: taskId, userId: user.id }));
  };
  useEffect(() => {
    dispatch(getUserList(localStorage.getItem('token')));
  }, []);
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          {auth.users.map((user, index) => (
            <div key={index}>
              <div className='flex items-center justify-between w-full'>
                <div>
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar src=''></Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      secondary={`@${user.fullName
                        .split(' ')
                        .join('_')
                        .toLowerCase()}`}
                      primary={user.fullName}
                    />
                  </ListItem>
                </div>
                <div>
                  <Button
                    className='customBtn'
                    onClick={() => handleAssignedTask(user)}
                  >
                    select
                  </Button>
                </div>
              </div>
              {index !== auth.users.length - 1 && <Divider variant='inset' />}
            </div>
          ))}
        </Box>
      </Modal>
    </div>
  );
}
