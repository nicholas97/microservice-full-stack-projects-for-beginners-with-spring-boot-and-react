import React, { useState } from 'react';
import './Auth.css';
import SignIn from '../Signin/SignIn';
import SignUp from '../SignUp/SignUp';

export default function Auth() {
  const [isRegister, setIsRegister] = useState(false);
  const togglePanel = () => {
    setIsRegister(!isRegister);
  };
  return (
    <div className='flex justify-center h-screen items-center overflow-hidden'>
      <div className='box lg:max-w-4x1'>
        <div className={`cover ${isRegister ? 'rotate-active' : ''}`}>
          <div className='front'>
            <img
              src='https://images.unsplash.com/photo-1484480974693-6ca0a78fb36b?q=80&w=2072&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
              alt=''
            />
            <div className='text'>
              <span className='text-1'>
                Success if built upon well-organized tasks
              </span>
              <span className='text-2 text-xs'>Let's get connected</span>
            </div>
          </div>
          <div className='back'>
            <img
              src='https://images.unsplash.com/photo-1590402494756-10c265b9d736?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
              alt=''
            />
          </div>
        </div>
        <div className='forms h-full'>
          <div className='form-content h-full'>
            <div className='login-form'>
              <SignIn togglePanel={togglePanel} />
            </div>
            <div className='signup-form'>
              <SignUp togglePanel={togglePanel} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
