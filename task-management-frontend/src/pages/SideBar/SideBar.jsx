import { Avatar, Button } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import './SideBar.css';
import TaskCreateForm from '../TaskCreateForm/TaskCreateForm';
import { logout } from '../../redux/AuthSlice';
import { getTasks } from '../../redux/TaskSlice';

const menu = [
  { name: 'Home', value: 'Home', roles: ['ROLE_ADMIN', 'ROLE_CUSTOMER'] },
  { name: 'DONE', value: 'DONE', roles: ['ROLE_ADMIN', 'ROLE_CUSTOMER'] },
  {
    name: 'ASSIGNED',
    value: 'ASSIGNED',
    roles: ['ROLE_ADMIN'],
  },
  {
    name: 'NOT ASSIGNED',
    value: 'PENDING',
    roles: ['ROLE_ADMIN'],
  },
  {
    name: 'Create New Task',
    value: 'Home',
    roles: ['ROLE_ADMIN'],
  },
  {
    name: 'Notification',
    value: 'Home',
    roles: ['ROLE_CUSTOMER'],
  },
];

export default function SideBar() {
  const dispatch = useDispatch();
  const { task, auth } = useSelector((store) => store);
  const location = useLocation();
  const navigate = useNavigate();
  const [activeMenu, setActiveMenu] = useState('Home');

  const [openTaskCreateForm, setOpenTaskCreateForm] = useState(false);
  const handleCloseTaskCreateForm = () => {
    setOpenTaskCreateForm(false);
  };
  const handleOpenCreateTask = () => {
    setOpenTaskCreateForm(true);
  };
  const handleMenuChange = (item) => {
    const updatedParams = new URLSearchParams(location.search);
    if (item.name === 'Create New Task') {
      handleOpenCreateTask();
    } else if (item.name === 'Home') {
      updatedParams.delete('filter');
      const queryString = updatedParams.toString();
      const updatedPath = queryString
        ? `${location.pathname}?${queryString}`
        : location.pathname;
      navigate(updatedPath);
    } else {
      updatedParams.set('filter', item.value);
      navigate(`${location.pathname}?${updatedParams.toString()}`);
    }
    setActiveMenu(item.name);
  };
  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <div>
      <div className='card min-h-[85vh] flex flex-col justify-center fixed w-[20vw]'>
        <div className='space-y-5 h-full'>
          <div className='flex justify-center'>
            <Avatar
              sx={{ width: '8rem', height: '8rem' }}
              className='border-2 border-[#c24dd0]'
              src=''
            />
          </div>
          {menu
            .filter((item) => item.roles.includes(auth.user.role))
            .map((item) => (
              <p
                key={item.name}
                onClick={() => handleMenuChange(item)}
                className={`py-3 px-5 rounded-full text-center cursor-pointer ${
                  activeMenu === item.name ? 'activeMenuItem' : 'menuItem'
                }`}
              >
                {item.name}
              </p>
            ))}
          <Button
            onClick={() => handleLogout()}
            sx={{ padding: '.7rem', borderRadius: '2rem' }}
            fullWidth
            className='logoutBtn'
          >
            Logout
          </Button>
        </div>
      </div>
      <TaskCreateForm
        handleClose={handleCloseTaskCreateForm}
        open={openTaskCreateForm}
      />
    </div>
  );
}
