import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IconButton, Menu, MenuItem } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import UserList from '../UserList/UserList';
import SubmissionList from '../SubmissionList/SubmissionList';
import SubmissionCreateForm from '../SubmissionCreateForm/SubmissionCreateForm';
import TaskUpdateForm from '../TaskUpdateForm/TaskUpdateForm';
import { deleteTask } from '../../redux/TaskSlice';
import { useLocation, useNavigate } from 'react-router-dom';

const role = 'ROLE_ADMIN';

export default function TaskCard({ task }) {
  const dispatch = useDispatch();
  const { auth } = useSelector((store) => store);
  const location = useLocation();
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = useState(null);
  const openMenu = Boolean(anchorEl);
  const handleRemoveTaskIdParams = () => {
    const updatedParams = new URLSearchParams(location.search);
    updatedParams.delete('taskId');
    const queryString = updatedParams.toString();
    const updatedPath = queryString
      ? `${location.pathname}?${queryString}`
      : location.pathname;
    navigate(updatedPath);
  };
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const [openUserList, setOpenUserList] = useState(false);
  const handleCloseUserList = () => {
    setOpenUserList(false);
    handleRemoveTaskIdParams();
  };
  const handleOpenUserList = () => {
    const updatedParams = new URLSearchParams(location.search);
    updatedParams.set('taskId', task.id);
    navigate(`${location.pathname}?${updatedParams.toString()}`);
    setOpenUserList(true);
    handleMenuClose();
  };

  const [openSubmissionList, setOpenSubmissionList] = useState(false);
  const handleCloseSubmissionList = () => {
    handleRemoveTaskIdParams();
    setOpenSubmissionList(false);
  };
  const handleOpenSubmissionList = () => {
    const updatedParams = new URLSearchParams(location.search);
    updatedParams.set('taskId', task.id);
    navigate(`${location.pathname}?${updatedParams.toString()}`);
    setOpenSubmissionList(true);
    handleMenuClose();
  };

  const [openTaskUpdateForm, setOpenTaskUpdateForm] = useState(false);
  const handleCloseTaskUpdateForm = () => {
    setOpenTaskUpdateForm(false);
    handleRemoveTaskIdParams();
  };
  const handleOpenUpdateTask = () => {
    const updatedParams = new URLSearchParams(location.search);
    updatedParams.set('taskId', task.id);
    navigate(`${location.pathname}?${updatedParams.toString()}`);
    setOpenTaskUpdateForm(true);
    handleMenuClose();
  };

  const handleOpenDeleteTask = () => {
    dispatch(deleteTask({ taskId: task.id }));
    handleMenuClose();
  };
  const [openSubmissionForm, setOpenSubmissionForm] = useState(false);
  const handleOpenSubmissionForm = () => {
    const updatedParams = new URLSearchParams(location.search);
    updatedParams.set('taskId', task.id);
    navigate(`${location.pathname}?${updatedParams.toString()}`);
    setOpenSubmissionForm(true);
  };
  const handleCloseSubmissionForm = () => {
    handleRemoveTaskIdParams();
    setOpenSubmissionForm(false);
  };
  return (
    <div>
      <div className='card lg:flex justify-between'>
        <div className='lg:flex gap-5 items-center space-y-2 w-[90%] lh:w-[70%]'>
          <div className=''>
            <img
              className='lg:w-[7ren] lg:h-[7rem] object-cover'
              src={task.image}
              alt=''
            />
          </div>
          <div className='space-y-5'>
            <div className='space-y-2'>
              <h1 className='font-bold text-lg'>{task.title}</h1>
              <p className='text-gray-500 text-sm'>{task.description}</p>
            </div>
            <div className='flex flex-wrap gap-2 items-center'>
              {task.tags.map((item) => (
                <span className='py-1 px-5 rounded-full techStack'>{item}</span>
              ))}
            </div>
          </div>
        </div>
        <div>
          <IconButton
            id='basic-button'
            aria-controls={openMenu ? 'basic-menu' : undefined}
            aria-haspopup='true'
            aria-expanded={openMenu ? 'true' : undefined}
            onClick={handleMenuClick}
          >
            <MoreVertIcon />
          </IconButton>
          <Menu
            id='basic-menu'
            anchorEl={anchorEl}
            open={openMenu}
            onClose={handleMenuClose}
            MenuListProps={{ 'aria-labelledby': 'basic-button' }}
          >
            {auth.user?.role === 'ROLE_ADMIN' ? (
              <>
                <MenuItem onClick={handleOpenUserList}>Assigned User</MenuItem>
                <MenuItem onClick={handleOpenSubmissionList}>
                  See Submission
                </MenuItem>
                <MenuItem onClick={handleOpenUpdateTask}>Edit</MenuItem>
                <MenuItem onClick={handleOpenDeleteTask}>Delete</MenuItem>
              </>
            ) : (
              <>
                <MenuItem onClick={handleOpenSubmissionForm}>Submit</MenuItem>
              </>
            )}
          </Menu>
        </div>
      </div>
      <UserList
        handleClose={handleCloseUserList}
        open={openUserList}
      />
      <SubmissionList
        handleClose={handleCloseSubmissionList}
        open={openSubmissionList}
      />
      <TaskUpdateForm
        handleClose={handleCloseTaskUpdateForm}
        open={openTaskUpdateForm}
      />
      <SubmissionCreateForm
        handleClose={handleCloseSubmissionForm}
        open={openSubmissionForm}
      />
    </div>
  );
}
