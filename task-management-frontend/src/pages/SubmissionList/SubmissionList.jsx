import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import SubmissionCard from '../SubmissionCard/SubmissionCard';
import { useLocation, useNavigate } from 'react-router-dom';
import { getTaskSubmissions } from '../../redux/SubmissionSlice';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function SubmissionList({ handleClose, open }) {
  const dispatch = useDispatch();
  const { submission } = useSelector((store) => store);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const taskId = queryParams.get('taskId');
  useEffect(() => {
    dispatch(getTaskSubmissions({ taskId: taskId }));
  }, [taskId]);

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <div>
            {submission.submissions.length > 0 ? (
              <div className='space-y-2'>
                {submission.submissions.map((submission) => (
                  <SubmissionCard submission={submission} />
                ))}
              </div>
            ) : (
              <div>
                <div className='text-center'>No Submission Found</div>
              </div>
            )}
          </div>
        </Box>
      </Modal>
    </div>
  );
}
