import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { Button, IconButton } from '@mui/material';
import { updateSubmissionStatus } from '../../redux/SubmissionSlice';

export default function SubmissionCard({ submission }) {
  const dispatch = useDispatch();
  const handleAcceptDecline = (status) => {
    dispatch(
      updateSubmissionStatus({ submissionId: submission.id, status: status })
    );
  };
  return (
    <div className='rounded-md bg-black p-5 flex items-center justify-between'>
      <div className='space-y-2'>
        <div className='flex items-center gap-2'>
          <span>Git :</span>
          <div className='flex items-center gap-2 text-[#c24dd0]'>
            <OpenInNewIcon />
            <a
              href={submission.gitLink}
              target='_blank'
              rel='noopener asdas'
            >
              Go To Link
            </a>
          </div>
        </div>
        <div className='flex items-center gap-2 text-xs'>
          <p>Submission Time:</p>
          <p className='text-gray-400'> {submission.submissionTime}</p>
        </div>
      </div>
      <div>
        {submission.status === 'PENDING' ? (
          <div className='flex gap-5'>
            <div className='text-green-500'>
              <IconButton
                color='success'
                onClick={() => handleAcceptDecline('ACCEPT')}
              >
                <CheckIcon />
              </IconButton>
            </div>
            <div className='text-red-500'>
              <IconButton
                color='error'
                onClick={() => handleAcceptDecline('DECLINED')}
              >
                <CloseIcon />
              </IconButton>
            </div>
          </div>
        ) : (
          <Button
            size='small'
            color={submission.status === 'ACCEPT' ? 'success' : 'error'}
            variant='outlined'
          >
            {submission.status === 'ACCEPT' ? 'Accept' : 'Declined'}
          </Button>
        )}
      </div>
    </div>
  );
}
