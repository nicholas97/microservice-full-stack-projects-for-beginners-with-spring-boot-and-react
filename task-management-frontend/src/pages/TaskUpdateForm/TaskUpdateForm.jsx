import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Modal,
  Box,
  Grid,
  TextField,
  Autocomplete,
  Button,
} from '@mui/material';
import { DateTimePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { getTask, updateTask } from '../../redux/TaskSlice';
import { useLocation, useNavigate } from 'react-router-dom';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  outline: 'none',
  boxShadow: 24,
  p: 4,
};

const tags = ['Angular', 'React', 'Vuejs', 'Spring boot', 'Node'];

export default function TaskUpdateForm({ handleClose, open }) {
  const dispatch = useDispatch();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const taskId = queryParams.get('taskId');
  const { task } = useSelector((store) => store);
  const [formData, setFormData] = useState({
    taskId: 0,
    title: '',
    image: '',
    description: '',
    tags: [],
    deadline: new Date(),
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };
  const handleTagsChange = (e, value) => {
    setFormData({ ...formData, tags: value });
  };

  const handleDeadlineChange = (date) => {
    setFormData({ ...formData, deadline: date });
  };

  const formatData = (input) => {
    let {
      $y: year,
      $M: month,
      $D: day,
      $H: hours,
      $m: minutes,
      $s: seconds,
      $ms: milliseconds,
    } = input;

    const date = new Date(
      year,
      month,
      day,
      hours,
      minutes,
      seconds,
      milliseconds
    );

    const formatedDate = date.toISOString();
    return formatedDate;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const { deadline } = formData;
    formData.taskId = taskId;
    formData.deadline = formatData(deadline);
    dispatch(updateTask({ ...formData }));

    console.log(formData, deadline);
    handleClose();
  };

  useEffect(() => {
    dispatch(getTask({ taskId: taskId }));
  }, [taskId]);
  useEffect(() => {
    if (task.taskDetails) {
      setFormData(task.taskDetails);
    }
  }, [task.taskDetails]);
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box sx={style}>
          <form onSubmit={handleSubmit}>
            <Grid
              container
              spacing={2}
              alignItems={'center'}
            >
              <Grid
                item
                xs={12}
              >
                <TextField
                  label={'Title'}
                  fullWidth
                  name='title'
                  value={formData.title}
                  onChange={handleChange}
                />
              </Grid>
              <Grid
                item
                xs={12}
              >
                <TextField
                  label={'Image'}
                  fullWidth
                  name='image'
                  value={formData.image}
                  onChange={handleChange}
                />
              </Grid>
              <Grid
                item
                xs={12}
              >
                <TextField
                  label={'Description'}
                  fullWidth
                  name='description'
                  value={formData.description}
                  onChange={handleChange}
                />
              </Grid>
              <Grid
                item
                xs={12}
              >
                <Autocomplete
                  multiple
                  id='multiple-limit-tags'
                  value={formData.tags}
                  options={tags}
                  onChange={handleTagsChange}
                  getOptionLabel={(option) => option}
                  renderInput={(params) => (
                    <TextField
                      label='Tags'
                      fullWidth
                      {...params}
                    />
                  )}
                />
              </Grid>
              <Grid
                item
                xs={12}
              >
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DateTimePicker
                    className='w-full'
                    label='Deadline'
                    onChange={handleDeadlineChange}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </LocalizationProvider>
              </Grid>
              <Grid
                item
                xs={12}
              >
                <Button
                  fullWidth
                  className='customBtn'
                  sx={{ padding: '.9rem' }}
                  type='submit'
                >
                  Update
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div>
  );
}
